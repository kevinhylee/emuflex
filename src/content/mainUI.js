
let Cu = Components.utils;
Cu.import("resource://gre/modules/XPCOMUtils.jsm");

const USER_AGENT_FIREFOX_MOBILE = "Mozilla/5.0 (Android; Mobile)";

if ("undefined" == typeof(Emuflex)) {
  var Emuflex = {};
};

Emuflex.UIManager = {

  init: function() {
    // call the Responsive Desgin View that comes with Firefox
    this.responsiveUIManager = ResponsiveUI.getResponsiveUIManager();
    this.responsiveUIManager.toggle(window, gBrowser.selectedTab);

    // set browser string
    this.setUserAgent();

    if (typeof(this.responsiveUIManager.isEmuflexActive) == "undefined"
        || !this.responsiveUIManager.isEmuflexActive) {
      // if Emuflex has not been started
      //this.extendUI();
      this.bound_extendUI = this.extendUI.bind(this);
      this.responsiveUIManager.on("on", this.bound_extendUI);
    } else {
      // if Emuflex has been started
      this.bound_close();
    }
  },

  extendUI: function() {
    this.responsiveUIManager.isEmuflexActive = true;

    this.mainWindow = window;
    this.browser = gBrowser.selectedTab.linkedBrowser;
    this.chromeDoc = window.document;
    this.container = gBrowser.getBrowserContainer(this.browser);
    this.stack = this.container.querySelector(".browserStack");

    // bind some functions
    this.bound_close = this.close.bind(this);
    this.bound_rotate = this.rotate.bind(this);
    this.bound_presetSelected = this.presetSelected.bind(this);
    this.bound_createCanvas = this.createCanvas.bind(this);
    this.bound_createCanvasWindow = this.createCanvasWindow.bind(this);
    this.bound_drawCanvas = this.drawCanvas.bind(this);
    this.bound_toggleBrowserMode = this.toggleBrowserMode.bind(this);
    this.bound_onMouseMove = this.onMouseMove.bind(this);

    // toolbar of the Responsive Design View
    this.toolbar = this.container.querySelector("toolbar");
    //this.toolbar.className = "devtools-toolbar devtools-responsiveui-toolbar";

    if (this.toolbar.children.length >= 2) {
      this.closebutton = this.toolbar.children[0];
      this.closebutton.addEventListener("command", this.bound_close);

      this.presets = this.toolbar.children[1];
      this.presets.addEventListener("command", this.bound_presetSelected);

      this.rotatebutton = this.toolbar.children[2];
      this.rotatebutton.addEventListener("command", this.bound_rotate);
    }

    if (this.toolbar.children.length >= 4) {
      // hide touch and screenshot buttons
      this.toolbar.children[3].hidden = true;
      this.toolbar.children[4].hidden = true;
    }

    this.asMobileBrowser = this.chromeDoc.createElement("checkbox");
    this.asMobileBrowser.setAttribute("label", "As Mobile Browser");
    this.asMobileBrowser.setAttribute("checked", "true");
    this.asMobileBrowser.addEventListener("command", this.bound_toggleBrowserMode);
    this.toolbar.appendChild(this.asMobileBrowser);

    // the hidden canvas in the emulator window
      this.canvas = this.bound_createCanvas();
    // the emulator window
    this.canvasWindow = this.bound_createCanvasWindow(this.canvas);
    this.canvasWindow.onbeforeunload = this.onBeforeCanvasWindowUnload.bind(this);
    this.stack.addEventListener("mousemove", this.bound_onMouseMove);

    // start updating the canvas in the emulator window
    this.bound_drawCanvas();
  },

  setUserAgent: function() {
    let preference = "general.useragent.override";
    let value = USER_AGENT_FIREFOX_MOBILE;

    let supportsStringInterface = Components.interfaces.nsISupportsString;
    let string = Components.classes["@mozilla.org/supports-string;1"].createInstance(supportsStringInterface);

    string.data = value;

    let prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("");
    prefs.setComplexValue(preference, supportsStringInterface, string);

  },

  removeUserAgent: function() {
    let prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("");
    let preference = "general.useragent.override";

    prefs.clearUserPref(preference);
  },

  toggleBrowserMode: function() {
    if (this.asMobileBrowser.checked) {
      this.setUserAgent();
    } else {
      this.removeUserAgent();
    }
  },

  close: function() {
    this.removeUserAgent();

    cancelAnimationFrame(this.drawCanvasId);
    this.canvas = null;
    this.canvasWindow.close();
    this.responsiveUIManager.isEmuflexActive = false;

    // remove event listeners
    this.closebutton.removeEventListener("command", this.bound_close);
    this.rotatebutton.removeEventListener("command", this.bound_rotate);
    this.stack.removeEventListener("mousemove", this.bound_onMouseMove);
  },

  rotate: function() {
    this.dispatchCustomEvent(this.canvasWindow, "emuflex_onrotate");
  },

  presetSelected: function() {
    this.dispatchCustomEvent(this.canvasWindow, "emuflex_onpresetselected");
  },

  onBeforeCanvasWindowUnload: function() {
    // before the emulator window is closed
    this.closebutton.click();
  },

  onMouseMove: function(event) {
    this.mousePosition = {
      x: event.clientX,
      y: event.clientY
    };
  },

  createCanvas: function() {
    let window = this.browser.contentWindow;
    let document = window.document;
    //let canvas = this.chromeDoc.createElementNS("http://www.w3.org/1999/xhtml", "canvas");
    let canvas = document.createElementNS("http://www.w3.org/1999/xhtml", "canvas");

    let width = window.innerWidth;
    let height = window.innerHeight;

    canvas.width = width;
    canvas.height = height;

    let ctx = canvas.getContext("2d");
    ctx.drawWindow(window, window.scrollX, window.scrollY, width, height, "#fff");

    canvas.style = "display:none;";

    return canvas;
  },

  createCanvasWindow: function(canvas) {
    // open the emulator window
    let canvasWindow = this.mainWindow.open("", "Emulator", "resizable=yes,scrollbars=yes,width=" + canvas.width + ",height=" + canvas.height);

    let doc = canvasWindow.document;

    canvas.setAttribute("id", "renderer");

    let js_threejs = doc.createElement("script");
    js_threejs.src = "chrome://emuflex/content/accessible/three.js";
    js_threejs.async = false;

    let js_transformControls = doc.createElement("script");
    js_transformControls.src = "chrome://emuflex/content/accessible/TransformControls.js";
    js_transformControls.async = false;

    let js_canvasWindow = doc.createElement("script");
    js_canvasWindow.src = "chrome://emuflex/content/accessible/canvasWindow.js";
    js_canvasWindow.async = false;

    let css_canvasWindow = doc.createElement("link");
    css_canvasWindow.href = "chrome://emuflex/content/accessible/canvasWindow.css";
    css_canvasWindow.rel = "stylesheet";

    let controlPanel = doc.createElement("div");
    controlPanel.setAttribute("id", "controlPanel");

    doc.body.appendChild(controlPanel);
    doc.body.appendChild(canvas);
    doc.head.appendChild(css_canvasWindow);
    doc.head.appendChild(js_threejs);
    doc.head.appendChild(js_transformControls);
    doc.head.appendChild(js_canvasWindow);

    return canvasWindow;
  },

  drawCanvas: function() {
    let window = this.browser.contentWindow;
    let width = window.innerWidth;
    let height = window.innerHeight;

    this.canvas.width = width;
    this.canvas.height = height;

    let ctx = this.canvas.getContext("2d");
    // put the content of the browser to the emulator
    ctx.drawWindow(window, window.scrollX, window.scrollY, width, height, "#fff");

    if (this.mousePosition) {

      // draw a cross on canvas
      let x = this.mousePosition.x;
      let y = this.mousePosition.y;
      let len_half = 10;

      ctx.beginPath();

      ctx.moveTo(x, y);
      ctx.lineTo(x, y + len_half);

      ctx.moveTo(x, y);
      ctx.lineTo(x, y - len_half);

      ctx.moveTo(x, y);
      ctx.lineTo(x + len_half, y);

      ctx.moveTo(x, y);
      ctx.lineTo(x - len_half, y);

      ctx.moveTo(x, y);

      ctx.closePath();

      ctx.strokeStyle = "rgba(0, 0, 0, 0.8)"
      ctx.stroke();
    }

    // resize the window to fit its content
    this.canvasWindow.sizeToContent();

    // keep drawing
    this.drawCanvasId = requestAnimationFrame(this.bound_drawCanvas);
  },

  dispatchCustomEvent: function(window, eventType, eventInitDict) {
    let doc = window.document;

    let _eventType = eventType || "custom";
    let _eventInitDict = eventInitDict || null;

    let event = doc.defaultView.CustomEvent(_eventType, _eventInitDict);
    doc.dispatchEvent(event);
  }
};

ResponsiveUI = {
  toggle: function() {
    this.ResponsiveUIManager.toggle(window, gBrowser.selectedTab);
  },

  getResponsiveUIManager: function() {
    return this.ResponsiveUIManager;
  }
};

XPCOMUtils.defineLazyGetter(ResponsiveUI, "ResponsiveUIManager", function() {
  let tmp = {};
  Cu.import("resource://devtools/client/responsivedesign/responsivedesign.jsm", tmp);
  return tmp.ResponsiveUIManager;
});


const ORIENTATION_PORTRAIT = 0;
const ORIENTATION_LANDSCAPE = 1;

const MODE_NORMAL = 0;
const MODE_CURVED = 1;
const MODE_SINE = 2;
const MODE_FLEXIBLE = 3;

var modes = [
  {
    label: "Normal"
  },
  {
    label: "Curved"
  },
  {
    label: "Sine Wave"
  },
  {
    label: "Flexible"
  }
];


var Painter = {
  orientation: ORIENTATION_PORTRAIT,
  currentMode: MODE_NORMAL,
  
  cameraSettings: {
    constant: 0,
    value: 0.5,
    maxValue: 1.0,
    minValue: 0.1,
    step: 0.01
  },
  
  curvature: {
    value: 5,
    maxValue: 10,
    minValue: -10,
    step: 1
  },
  
  sine: {
    f: 0.02,
    f_max: 0.1,
    f_min: 0.01,
    f_step: 0.01,
    
    phase: Math.PI,
    phase_max: Math.PI,
    phase_min: 0,
    phase_step: Math.round(100 * Math.PI / 180) / 100
  },
  
  flexible: {
    // (n + 1) rows, (m + 1) cols
    // There should be (n + 1) * (m + 1) control points
    degree: {n: 3, m: 3},
    
    ctrlPts: [],
    
    // 1D array of ctrlPts
    objects: [],
    
    ctrlPtsInitialized: false
  },
  
  init: function() {
    // preserve previous rotation and position information
    if (this.plane && this.plane.rotation) {
      this.rotation = this.plane.rotation;
    }
    if (this.camera && this.camera.position) {
      this.cameraPosition = this.camera.position;
    }
    
    // bind functions
    this.bound_render = this.render.bind(this);
    this.bound_onRotate = this.onRotate.bind(this);
    this.bound_onPresetSelected = this.onPresetSelected.bind(this);
    this.bound_destroy = this.destroy.bind(this);
    this.bound_redraw = this.redraw.bind(this);
    this.bound_addControlPoints = this.addControlPoints.bind(this);
    this.bound_showControlPoints = this.showControlPoints.bind(this);
    this.bound_hideControlPoints = this.hideControlPoints.bind(this);
    this.bound_rotateControlPoints = this.rotateControlPoints.bind(this);
    this.bound_removeControlPoints = this.removeControlPoints.bind(this);
    this.bound_setMode = this.setMode.bind(this);
    this.bound_updateMode = this.updateMode.bind(this);
    
    
    this.canvas = document.querySelector("#renderer");
    this.oldCanvasWidth = this.canvas.width;
    this.oldCanvasHeight = this.canvas.height;
    this.newCanvasWidth = this.canvas.width;
    this.newCanvasHeight = this.canvas.height;
    
    this.texture = new THREE.Texture(this.canvas);
    
    var browserWidth = Math.round(this.canvas.width / 10);
    var browserHeight = Math.round(this.canvas.height / 10);
    var browserWidthSegments = browserWidth;
    var browserHeightSegments = browserHeight;
    
    var rendererWidth = this.canvas.width;
    var rendererHeight = this.canvas.height;
    
    var fov = 75;
    var aspectRatio = rendererWidth / rendererHeight;
    var nearPlane = 0.1;
    var farPlane = 1000;
    
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(fov, aspectRatio, nearPlane, farPlane);
    
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(rendererWidth, rendererHeight);
    this.rendererCanvas = this.renderer.domElement;
    document.body.appendChild(this.rendererCanvas);
    
    var geometry = new THREE.PlaneGeometry(browserWidth, browserHeight, browserWidthSegments, browserHeightSegments);
    this.processVertices(geometry);
    
    var materials = [];
    materials.push(new THREE.MeshLambertMaterial({ map: this.texture, side: THREE.FrontSide }));
    materials.push(new THREE.MeshBasicMaterial({ color: 0x282828, side: THREE.BackSide }));
    
    var material = new THREE.MeshFaceMaterial(materials);
    
    // show two sides of the plane
    for (var i = 0, len = geometry.faces.length; i < len; i++) {
      var face = geometry.faces[i].clone();
      face.materialIndex = 1;
      geometry.faces.push(face);
      geometry.faceVertexUvs[0].push(geometry.faceVertexUvs[0][i].slice(0));
    }
    
    this.plane = new THREE.Mesh(geometry, material);
    this.plane.rotation = this.rotation || new THREE.Euler(0, 0, 0, "XYZ");
    this.scene.add(this.plane);
    
    // add ambient lighting
    var ambientLight = new THREE.AmbientLight(0xffffff);
    this.scene.add(ambientLight);
    
    this.cameraSettings.constant = 2 * browserHeight;
    this.camera.position = this.cameraPosition || new THREE.Vector3(0, 0, this.cameraSettings.constant * (1 - this.cameraSettings.value));
    
    // attach event listners
    window.document.addEventListener("emuflex_onrotate", this.bound_onRotate);
    window.document.addEventListener("emuflex_onpresetselected", this.bound_onPresetSelected);
    
    this.bound_render();
  },
  
  addControlPoints: function() {
    
    if (this.flexible.ctrlPtsInitialized) {
      return;
    }
    
    var geometry = this.plane.geometry;
    
    var width = geometry.width;
    var height = geometry.height;
    
    var widthSegments = geometry.widthSegments;
    var heightSegments = geometry.heightSegments;
    
    var gridX = widthSegments;
    var gridZ = heightSegments;
    
    var gridX1 = gridX + 1;
    var gridZ1 = gridZ + 1;
    
    var gridX1_half = Math.floor(gridX1 / 2);
    var gridZ1_half = Math.floor(gridZ1 / 2);
    
    
    this.control = new THREE.TransformControls(this.camera, this.rendererCanvas);
    this.control.addEventListener("change", function() {
      // update vertices
      Painter.bound_updateMode();
      
    });
    
    // (n + 1) rows, (m + 1) cols
    // There should be (n + 1) * (m + 1) control points
    this.flexible.ctrlPts = [];
    var i, j, len_i, len_j, x, y,
        ix, iz, index,
        ctrlPt_geometry, ctrlPt_material, ctrlPt_mesh,
        ctrlPt_radius = 0.5, ctrlPt_color = 0xff0000;
    
    var delta = {
      x: gridX / this.flexible.degree.m,
      y: gridZ / this.flexible.degree.n,
      z: 0
    };
    
    for (j = 0, len_j = this.flexible.degree.n + 1; j < len_j; j++) {
      
      this.flexible.ctrlPts.push([]);
      
      for (i = 0, len_i = this.flexible.degree.m + 1; i < len_i; i++) {
        
        ctrlPt_geometry = new THREE.SphereGeometry(ctrlPt_radius);
        ctrlPt_material = new THREE.MeshBasicMaterial({color: ctrlPt_color});
        ctrlPt_mesh = new THREE.Mesh(ctrlPt_geometry, ctrlPt_material);
        
        ctrlPt_mesh.translateX(delta.x * i - gridX1_half);
        ctrlPt_mesh.translateY(gridZ1_half - delta.y * j);
        ctrlPt_mesh.translateZ(delta.z);
        this.plane.add(ctrlPt_mesh);
        
        this.flexible.ctrlPts[j].push(ctrlPt_mesh);
        this.flexible.objects.push(ctrlPt_mesh);
      }
      
    }
    
    this.scene.add(this.control);
    this.flexible.ctrlPtsInitialized = true;
  },
  
  rotateControlPoints: function() {
    var degree = {n: this.flexible.degree.n, m: this.flexible.degree.m};
    this.flexible.degree = {n: degree.m, m: degree.n};
    
    this.bound_removeControlPoints();
    this.bound_addControlPoints();
  },
  
  showControlPoints: function() {
    if (!this.flexible.ctrlPtsInitialized) {
      return;
    }
    var i, len;
    for (i = 0, len = this.flexible.objects.length; i < len; i++) {
      if (!this.plane.getObjectById(this.flexible.objects[i].id)) {
        this.plane.add(this.flexible.objects[i]);
      }
    }
    if (!this.scene.getObjectById(this.control.id)) {
      this.scene.add(this.control);
    }
  },
  
  hideControlPoints: function() {
    if (!this.flexible.ctrlPtsInitialized) {
      return;
    }
    var i, len;
    for (i = 0, len = this.flexible.objects.length; i < len; i++) {
      if (this.plane.getObjectById(this.flexible.objects[i].id)) {
        this.plane.remove(this.flexible.objects[i]);
      }
    }
    if (this.scene.getObjectById(this.control.id)) {
      this.scene.remove(this.control);
    }
  },
  
  removeControlPoints: function() {
    var i, len;
    
    
    for (i = 0, len = this.flexible.objects.length; i < len; i++) {
      this.plane.remove(this.flexible.objects[i]);
    }
    
    this.flexible.ctrlPts = [];
    this.flexible.objects = [];
    this.control.removeEventListener("change");
    
    this.scene.remove(this.control);
    this.flexible.ctrlPtsInitialized = false;
  },
  
  setMode: function(mode) {
    this.currentMode = parseInt(mode);
  },
  
  updateMode: function() {
    this.processVertices(this.plane.geometry);
    this.plane.geometry.verticesNeedUpdate = true;
  },
  
  processVertices: function(geometry) {
    
    var mode = this.currentMode;
    
    var isDecreasingCurvature = Control.decreasingCurvature || false;
    
    var width = geometry.width;
    var height = geometry.height;
    
    var widthSegments = geometry.widthSegments;
    var heightSegments = geometry.heightSegments;
    
    var gridX = widthSegments;
    var gridZ = heightSegments;
    
    var gridX1 = gridX + 1;
    var gridZ1 = gridZ + 1;
    
    var gridX1_half = Math.floor(gridX1 / 2);
    var gridZ1_half = Math.floor(gridZ1 / 2);
    
    var grid,
        grid1,
        grid1_half,
        deviceLength,
        numSegments;
    
    if (this.orientation === ORIENTATION_PORTRAIT) {
      grid = gridZ;
      grid1 = gridZ1;
      grid1_half = gridZ1_half;
      deviceLength = height;
      numSegments = heightSegments;
    } else {
      grid = gridX;
      grid1 = gridX1;
      grid1_half = gridX1_half;
      deviceLength = width;
      numSegments = widthSegments;
    }
    
    var i,
        j,
        ix,
        iz,
        index,
        t,
        f = 0.5 / grid1,
        a = this.curvature.value;
    
    var z_old = 0,
        z_new = 0,
        arclen = 0,
        delta = deviceLength / numSegments,
        i_upper = 0,
        i_upperFound = false;
    
    var resetPlane = function() {
      
      for (iz = 0; iz < gridZ1; iz++) {
        for (ix = 0; ix < gridX1; ix++) {
          index = ix + iz * gridX1;
          
          geometry.vertices[index].x = ix - gridX1_half;
          geometry.vertices[index].y = gridZ1_half - iz;
          geometry.vertices[index].z = 0;
          
        }
      }
      
    };
    
    switch (parseInt(mode)) {
      case MODE_NORMAL:
        resetPlane();
        break;
      
      case MODE_CURVED:
        resetPlane();
        // update z positions
        for (iz = 0; iz < gridZ1; iz++) {
          for (ix = 0; ix < gridX1; ix++) {
            index = ix + iz * gridX1;
            i = this.orientation === ORIENTATION_PORTRAIT ? iz : ix;
            j = this.orientation === ORIENTATION_PORTRAIT ? ix : iz;
            t = 2 * Math.PI * f * i - Math.PI;
            
            // calculate the arc lenght of the curve
            // for adjusting y positions
            if (j === 0) {
              if (!i_upperFound && a == 0) {
                i_upper = grid;
                i_upperFound = true;
              } else if (!i_upperFound && arclen > deviceLength) {
                i_upper = i - 1;
                i_upperFound = true;
              } else {
                // approximation to the arc length
                z_old = z_new;
                z_new = a * Math.sin(t);
                arclen += Math.sqrt(delta * delta + (z_new - z_old) * (z_new - z_old));
              }
            }
            
            geometry.vertices[index].z = a * Math.sin(t);
          }
        }
        
        // update y positions
        for (iz = 0; iz < gridZ1; iz++) {
          for (ix = 0; ix < gridX1; ix++) {
            index = ix + iz * gridX1;
            i = this.orientation === ORIENTATION_PORTRAIT ? iz : ix;
            t = isDecreasingCurvature ? (grid1_half - i) / grid1 : (i - grid1_half) / grid1;
            
            if (a < 0) {
              t = -t;
            }
            if (this.orientation === ORIENTATION_PORTRAIT) {
              geometry.vertices[index].y += (grid - i_upper) * t / (2 * delta);
            } else {
              geometry.vertices[index].x -= (grid - i_upper) * t / (2 * delta);
            }
            
          }
        }
        break;
      
      case MODE_SINE:
        resetPlane();
        // update z positions
        for (iz = 0; iz < gridZ1; iz++) {
          for (ix = 0; ix < gridX1; ix++) {
            index = ix + iz * gridX1;
            i = this.orientation === ORIENTATION_PORTRAIT ? iz : ix;
            j = this.orientation === ORIENTATION_PORTRAIT ? ix : iz;
            t = 2 * Math.PI * this.sine.f * i - this.sine.phase;
            
            geometry.vertices[index].z = a * Math.sin(t);
          }
        }
        
        break;
      
      case MODE_FLEXIBLE:
        resetPlane();
        
        
        /*
         * returns an interpolated value in the Bezier surface
         * 
         * degree = {n, m}: n rows, m columns
         * t = {u, v}
         * crtlPts = n by m matrix
         * 
         */
        var getBezier = function(degree, t, ctrlPts) {
          
          // binomial coefficient
          var choose = function(n, k) {
              
            var factorial = function fact(k) {
              if (k < 2) {
                return 1;
              }
              return k * fact(k - 1);
            };
            
            return factorial(n) / factorial(k) / factorial(n - k);
          }
          
          var C = ctrlPts;
          var i, j, len_i, len_j;
          var n = degree.n, m = degree.m, u = t.u, v = t.v;
          var B_n = [], B_m = [];
          
          for (i = 0, len_i = n + 1; i < len_i; i++) {
            B_n.push(
              function(u) {
                return choose(n, i) * Math.pow(1 - u, n - i) * Math.pow(u, i);
              }
            );
          }
          
          for (j = 0, len_j = m + 1; j < len_j; j++) {
            B_m.push(
              function(v) {
                return choose(m, j) * Math.pow(1 - v, m - j) * Math.pow(v, j);
              }
            );
          }
          
          var x = 0, y = 0, z = 0, b_i, b_j;
          
          for (i = 0, len_i = n + 1; i < len_i; i++) {
            
            b_i = B_n[i](u);
            
            for (j = 0, len_j = m + 1; j < len_j; j++) {
              
              b_j = B_m[j](v);
              
              x += C[i][j].x * b_i * b_j;
              y += C[i][j].y * b_i * b_j;
              z += C[i][j].z * b_i * b_j;
              
            }
            
          }
          
          return new THREE.Vector3(x, y, z);
        };
        
        
        var ix, iz, index, i, j, len_i, len_j;
        var t = {u:0, v:0};
        var degree = Painter.flexible.degree;
        
        // There should be (n + 1) * (m + 1) control points
        var ctrlPts_position = [];
        for (j = 0, len_j = this.flexible.ctrlPts.length; j < len_j; j++) {
          ctrlPts_position.push([]);
          for (i = 0, len_i = this.flexible.ctrlPts[j].length; i < len_i; i++) {
            ctrlPts_position[j].push(this.flexible.ctrlPts[j][i].position);
          }
        }
        
        for (ix = 0; ix < gridX1; ix++) {
          for (iz = 0; iz < gridZ1; iz++) {
            index = ix + iz * gridX1;
            
            t.u = iz / gridZ;
            t.v = ix / gridX;
            
            var pos = getBezier(degree, t, ctrlPts_position);
            
            geometry.vertices[index].x = pos.x;
            geometry.vertices[index].y = pos.y;
            geometry.vertices[index].z = pos.z;
          }
        }
        
        break;
    }
    
  },
  
  render: function() {
    if (this.canvas.width != this.oldCanvasWidth || this.canvas.height != this.oldCanvasHeight) {
      // wait until the canvas is fully resized
      if (this.canvas.width == this.newCanvasWidth && this.canvas.height == this.newCanvasHeight) {
        this.bound_redraw();
        if (Control.isFlexibleModeEnabled) {
          this.bound_rotateControlPoints();
        }
        this.processVertices(this.plane.geometry);
        return;
      }
      
      this.newCanvasWidth = this.canvas.width;
      this.newCanvasHeight = this.canvas.height;
    }
    
    if (Control.isFlexibleModeEnabled) {
      // update the geometry
      this.plane.geometry.verticesNeedUpdate = true;
      
      // if the transform control exists, update its position
      if (this.control) {
        // update control position
        this.control.update();
      }
      
    }
    
    // UI events
    if (Control.curvatureChanged) {
      if (this.curvature.value > this.curvature.maxValue) {
        this.curvature.value = this.curvature.maxValue;
      }
      if (this.curvature.value < this.curvature.minValue) {
        this.curvature.value = this.curvature.minValue;
      }
      
      this.processVertices(this.plane.geometry);
      this.plane.geometry.verticesNeedUpdate = true;
      
      Control.curvatureChanged = false;
    }
    
    if (Control.cameraPositionChanged) {
      if (this.cameraSettings.value > this.cameraSettings.maxValue) {
        this.cameraSettings.value = this.cameraSettings.maxValue;
      }
      if (this.cameraSettings.value < this.cameraSettings.minValue) {
        this.cameraSettings.value = this.cameraSettings.minValue;
      }
      
      this.camera.position.z = this.cameraSettings.constant * (1 - this.cameraSettings.value);
      this.camera.updateProjectionMatrix();
      Control.cameraPositionChanged = false;
    }
    
    if (Control.freqChanged) {
      if (this.sine.f > this.sine.f_max) {
        this.sine.f = this.sine.f_max;
      }
      if (this.sine.f < this.sine.f_min) {
        this.sine.f = this.sine.f_min;
      }
      
      this.processVertices(this.plane.geometry);
      this.plane.geometry.verticesNeedUpdate = true;
      
      Control.freqChanged = false;
    }
    
    if (Control.phaseChanged) {
      if (this.sine.phase > this.sine.phase_max) {
        this.sine.phase = this.sine.phase_max;
      }
      if (this.sine.phase < this.sine.phase_min) {
        this.sine.phase = this.sine.phase_min;
      }
      
      this.processVertices(this.plane.geometry);
      this.plane.geometry.verticesNeedUpdate = true;
      
      Control.phaseChanged = false;
    }
    
    // keyboard events
    if (Control.isRotating.left) {
      this.plane.rotation.y -= 0.1;
    }
    if (Control.isRotating.up) {
      this.plane.rotation.x -= 0.1;
    }
    if (Control.isRotating.right) {
      this.plane.rotation.y += 0.1;
    }
    if (Control.isRotating.down) {
      this.plane.rotation.x += 0.1;
    }
    
    this.texture.needsUpdate = true;
    this.renderer.render(this.scene, this.camera);
    this.requestID = requestAnimationFrame(this.bound_render);
  },
  
  onRotate: function() {
    this.orientation = (this.orientation + 1) % 2;
  },
  
  onPresetSelected: function() {
    this.orientation = ORIENTATION_PORTRAIT;
    
    if (Control.isFlexibleModeEnabled) {
      
      var showControlPoints = document.getElementById("showControlPoints");
      
      if (!showControlPoints.checked) {
        showControlPoints.checked = true;
        Control.isRotationEnabled = false;
      }
      
    }
    
  },
  
  destroy: function() {
    if (this.renderer && this.renderer.domElement) {
      window.document.body.removeChild(this.renderer.domElement);
    }
    window.cancelAnimationFrame(this.requestID);
    window.document.removeEventListener("emuflex_onrotate", this.bound_onRotate);
    window.document.removeEventListener("emuflex_onpresetselected", this.bound_onPresetSelected);
  },
  
  redraw: function() {
    this.destroy();
    Control.bound_unregisterEventListeners();
    
    this.init();
    Control.bound_registerEventListeners();
  }
};


var Control = {
  movementSpeed: 0.01,
  
  isRotationEnabled: true,
  
  isFlexibleModeEnabled: false,
  
  isMouseDown: false,
  
  curvatureChanged: false,
  
  freqChanged: false,
  
  phaseChanged: false,
  
  cameraPositionChanged: false,
  
  isRotating: {
    left: false,
    right: false,
    up: false,
    down: false
  },
  
  getMousePosition: function(event) {
    var rendererCanvas = Painter.rendererCanvas;
    var camera = Painter.camera;
    var plane = Painter.plane;
    
    var rect = rendererCanvas.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    
    var vector = new THREE.Vector3((x / rendererCanvas.width) * 2 - 1,
                                   -(y / rendererCanvas.height) * 2 + 1,
                                   0.5);
    
    var projector = new THREE.Projector();
    projector.unprojectVector(vector, camera);
    
    var dir = vector.sub(camera.position).normalize();
    
    var distance = -camera.position.z / dir.z;
    
    var pos = camera.position.clone().add(dir.multiplyScalar(distance));
    
    return pos;
  },
  
  onMouseUp: function(event) {
    this.isMouseDown = false;
  },
  
  onMouseDown: function(event) {
    this.isMouseDown = true;
    
    if (Painter.currentMode === MODE_FLEXIBLE && !this.isRotationEnabled) {
      // select a control point and attach the control to it
      var rendererCanvas = Painter.rendererCanvas;
      var camera = Painter.camera;
      var control = Painter.control;
      
      var objects = Painter.flexible.objects;
      
      var rect = rendererCanvas.getBoundingClientRect();
      var x = event.clientX - rect.left;
      var y = event.clientY - rect.top;
      
      var vector = new THREE.Vector3((x / rendererCanvas.width) * 2 - 1,
                                     -(y / rendererCanvas.height) * 2 + 1,
                                     0.5);
      
      var projector = new THREE.Projector();
      projector.unprojectVector(vector, camera);
      
      var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
      
      var intersects = raycaster.intersectObjects(objects);
      
      if (intersects.length > 0) {
        
        intersects[0].object.material.color.setHex(0x00ff00);
        control.attach(intersects[0].object);
        
      }
    }
    
  },
  
  onMouseMove: function(event) {
    
    if (this.isMouseDown && this.isRotationEnabled) {
      
      var movementX = event.movementX || event.mozMovementX || 0;
      var movementY = event.movementY || event.mozMovementY || 0;
      
      Painter.plane.rotation.y += movementX * this.movementSpeed;
      Painter.plane.rotation.x += movementY * this.movementSpeed;
      
    }
    
  },
  
  onKeyUp: function(event) {
    switch (event.keyCode) {
      
      // left arrow
      case 37:
        this.isRotating.left = false;
        break;
      
      // up arrow
      case 38:
        this.isRotating.up = false;
        break;
      
      // right arrow
      case 39:
        this.isRotating.right = false;
        break;
      
      // down arrow
      case 40:
        this.isRotating.down = false;
        break;
    }
  },
  
  onKeyDown: function(event) {
    switch (event.keyCode) {
      
      // left arrow
      case 37:
        this.isRotating.left = true;
        break;
      
      // up arrow
      case 38:
        this.isRotating.up = true;
        break;
      
      // right arrow
      case 39:
        this.isRotating.right = true;
        break;
      
      // down arrow
      case 40:
        this.isRotating.down = true;
        break;
      
    }
  },
  
  setText: function(node, text) {
    while (node.firstChild) {
      node.removeChild(node.firstChild);
    }
    node.appendChild(document.createTextNode(text));
  },
  
  onZoomChange: function(event) {
    var newValue = event.target.value;
    this.cameraPositionChanged = true;
    Painter.cameraSettings.value = newValue;
    this.setText(document.getElementById("zoom-slider"), newValue);
  },
  
  onCurvatureChange: function(event) {
    var newValue = event.target.value;
    this.curvatureChanged = true;
    Painter.curvature.value = newValue;
    this.setText(document.getElementById("curvature-slider"), newValue);
  },
  
  onFreqChange: function(event) {
    var newValue = event.target.value;
    this.freqChanged = true;
    Painter.sine.f = newValue;
    this.setText(document.getElementById("frequency-slider"), newValue);
  },
  
  onPhaseChange: function(event) {
    var newValue = Math.round(event.target.value * 100) / 100;
    this.phaseChanged = true;
    Painter.sine.phase = newValue;
    this.setText(document.getElementById("phase-slider"), newValue);
  },
  
  onShowControlPointsClick: function(event) {
    this.isRotationEnabled = !this.isRotationEnabled;
    var txt = "";
    if (this.isRotationEnabled) {
      Painter.bound_hideControlPoints();
      txt = "No";
    } else {
      Painter.bound_showControlPoints();
      txt = "Yes";
    }
    this.setText(document.getElementById("show-control-points"), txt);
  },
  
  onModeChange: function(event) {
    var mode = event.target.value;
    var curvatureSlider, freqSlider, phaseSlider, showControlPoints;
    
    // clear the previous mode settings
    switch (Painter.currentMode) {
      case MODE_NORMAL:
        
        break;
      
      case MODE_CURVED:
        curvatureSlider = document.getElementById("curvatureSlider");
        curvatureSlider.setAttribute("disabled", "disabled");
        curvatureSlider.parentNode.parentNode.setAttribute("class", "disabled");
        break;
      
      case MODE_SINE:
        curvatureSlider = document.getElementById("curvatureSlider");
        curvatureSlider.setAttribute("disabled", "disabled");
        curvatureSlider.parentNode.parentNode.setAttribute("class", "disabled");
        
        freqSlider = document.getElementById("freqSlider");
        freqSlider.setAttribute("disabled", "disabled");
        freqSlider.parentNode.parentNode.setAttribute("class", "disabled");
        
        phaseSlider = document.getElementById("phaseSlider");
        phaseSlider.setAttribute("disabled", "disabled");
        phaseSlider.parentNode.parentNode.setAttribute("class", "disabled");
        break;
      
      case MODE_FLEXIBLE:
        Painter.bound_removeControlPoints();
        
        this.isRotationEnabled = true;
        this.isFlexibleModeEnabled = false;
        
        showControlPoints = document.getElementById("showControlPoints");
        showControlPoints.setAttribute("disabled", "disabled");
        showControlPoints.parentNode.parentNode.setAttribute("class", "disabled");
        break;
    }
    
    // change mode
    Painter.bound_setMode(mode);
    // update label
    this.setText(document.getElementById("mode-menu"), modes[mode].label);
    
    switch (Painter.currentMode) {
      case MODE_NORMAL:
        this.isRotationEnabled = true;
        break;
      
      case MODE_CURVED:
        this.isRotationEnabled = true;
        curvatureSlider = document.getElementById("curvatureSlider");
        curvatureSlider.removeAttribute("disabled");
        curvatureSlider.parentNode.parentNode.setAttribute("class", "");
        break;
      
      case MODE_SINE:
        this.isRotationEnabled = true;
        
        curvatureSlider = document.getElementById("curvatureSlider");
        curvatureSlider.removeAttribute("disabled");
        curvatureSlider.parentNode.parentNode.setAttribute("class", "");
        
        freqSlider = document.getElementById("freqSlider");
        freqSlider.removeAttribute("disabled");
        freqSlider.parentNode.parentNode.setAttribute("class", "");
        
        phaseSlider = document.getElementById("phaseSlider");
        phaseSlider.removeAttribute("disabled");
        phaseSlider.parentNode.parentNode.setAttribute("class", "");
        break;
      
      case MODE_FLEXIBLE:
        Painter.bound_addControlPoints();
        
        this.isRotationEnabled = false;
        this.isFlexibleModeEnabled = true;
        
        showControlPoints = document.getElementById("showControlPoints");
        showControlPoints.removeAttribute("disabled");
        showControlPoints.checked = true;
        showControlPoints.parentNode.parentNode.setAttribute("class", "");
        break;
    }
    
    Painter.bound_updateMode(mode);
  },
  
  registerEventListeners: function() {
    Painter.rendererCanvas.addEventListener("mouseup", this.bound_onMouseUp);
    Painter.rendererCanvas.addEventListener("mousedown", this.bound_onMouseDown);
    Painter.rendererCanvas.addEventListener("mousemove", this.bound_onMouseMove);
    window.document.addEventListener("keyup", this.bound_onKeyUp);
    window.document.addEventListener("keydown", this.bound_onKeyDown);
  },
  
  unregisterEventListeners: function() {
    Painter.rendererCanvas.removeEventListener("mouseup", this.bound_onMouseUp);
    Painter.rendererCanvas.removeEventListener("mousedown", this.bound_onMouseDown);
    Painter.rendererCanvas.removeEventListener("mousemove", this.bound_onMouseMove);
    window.document.removeEventListener("keyup", this.bound_onKeyUp);
    window.document.removeEventListener("keydown", this.bound_onKeyDown);
  },
  
  buildControlPanel: function() {
    var panel = document.getElementById("controlPanel");
    if (!panel) {
      return;
    }
    
    var controls = [];
    
    // mode menu
    var modeMenu = document.createElement("select");
    var i, len, menuItem;
    for (i = 0, len = modes.length; i < len; i++) {
      menuItem = document.createElement("option");
      menuItem.setAttribute("label", modes[i].label);
      menuItem.setAttribute("value", i);
      menuItem.appendChild(document.createTextNode(modes[i].label));
      
      modeMenu.appendChild(menuItem);
    }
    modeMenu.addEventListener("change", this.bound_onModeChange);
    controls.push({
      id: "mode-menu",
      label: "Mode",
      control: modeMenu,
      value: modes[0].label
    });
    
    // zoom
    var zoomSlider = document.createElement("input");
    zoomSlider.setAttribute("type", "range");
    zoomSlider.setAttribute("min", Painter.cameraSettings.minValue);
    zoomSlider.setAttribute("max", Painter.cameraSettings.maxValue);
    zoomSlider.setAttribute("step", Painter.cameraSettings.step);
    zoomSlider.setAttribute("value", Painter.cameraSettings.value);
    zoomSlider.addEventListener("input", this.bound_onZoomChange);
    controls.push({
      id: "zoom-slider",
      label: "Zoom",
      control: zoomSlider,
      value: Painter.cameraSettings.value
    });
    
    // curvature
    var curvatureSlider = document.createElement("input");
    curvatureSlider.setAttribute("id", "curvatureSlider");
    curvatureSlider.setAttribute("type", "range");
    curvatureSlider.setAttribute("min", Painter.curvature.minValue);
    curvatureSlider.setAttribute("max", Painter.curvature.maxValue);
    curvatureSlider.setAttribute("step", Painter.curvature.step);
    curvatureSlider.setAttribute("value", Painter.curvature.value);
    curvatureSlider.setAttribute("disabled", "disabled");
    curvatureSlider.addEventListener("input", this.bound_onCurvatureChange);
    controls.push({
      id: "curvature-slider",
      label: "Curvature",
      control: curvatureSlider,
      value: Painter.curvature.value
    });
    
    // sine wave
    var freqSlider = document.createElement("input");
    freqSlider.setAttribute("id", "freqSlider");
    freqSlider.setAttribute("type", "range");
    freqSlider.setAttribute("min", Painter.sine.f_min);
    freqSlider.setAttribute("max", Painter.sine.f_max);
    freqSlider.setAttribute("step", Painter.sine.f_step);
    freqSlider.setAttribute("value", Painter.sine.f);
    freqSlider.setAttribute("disabled", "disabled");
    freqSlider.addEventListener("input", this.bound_onFreqChange);
    controls.push({
      id: "frequency-slider",
      label: "Frequency",
      control: freqSlider,
      value: Painter.sine.f
    });
    
    var phaseSlider = document.createElement("input");
    phaseSlider.setAttribute("id", "phaseSlider");
    phaseSlider.setAttribute("type", "range");
    phaseSlider.setAttribute("min", Painter.sine.phase_min);
    phaseSlider.setAttribute("max", Painter.sine.phase_max);
    phaseSlider.setAttribute("step", Painter.sine.phase_step);
    phaseSlider.setAttribute("value", Painter.sine.phase);
    phaseSlider.setAttribute("disabled", "disabled");
    phaseSlider.addEventListener("input", this.bound_onPhaseChange);
    controls.push({
      id: "phase-slider",
      label: "Phase",
      control: phaseSlider,
      value: Math.round(Painter.sine.phase * 100) / 100
    });
    
    var showControlPoints = document.createElement("input");
    showControlPoints.setAttribute("id", "showControlPoints");
    showControlPoints.setAttribute("type", "checkbox");
    showControlPoints.checked = true;
    showControlPoints.setAttribute("disabled", "disabled");
    showControlPoints.addEventListener("click", this.bound_onShowControlPointsClick);
    controls.push({
      id: "show-control-points",
      label: "Show control points",
      control: showControlPoints,
      value: "Yes"
    });
    
    // table
    var table = document.createElement("table");
    table.setAttribute("id", "panel-table");
    var tr, td;
    
    for (i = 0, len = controls.length; i < len; i++) {
      tr = document.createElement("tr");
      td = document.createElement("td");
      
      td.appendChild(document.createTextNode(controls[i].label + ":"));
      tr.appendChild(td);
      
      td = document.createElement("td");
      td.appendChild(controls[i].control);
      tr.appendChild(td);
      
      td = document.createElement("td");
      td.setAttribute("id", controls[i].id);
      td.appendChild(document.createTextNode(controls[i].value));
      tr.appendChild(td);
      
      table.appendChild(tr);
    }
    panel.appendChild(table);
    
    // hide some controls
    curvatureSlider.parentNode.parentNode.setAttribute("class", "disabled");
    freqSlider.parentNode.parentNode.setAttribute("class", "disabled");
    phaseSlider.parentNode.parentNode.setAttribute("class", "disabled");
    showControlPoints.parentNode.parentNode.setAttribute("class", "disabled");
    
  },
  
  init: function() {
    this.bound_getMousePosition = this.getMousePosition.bind(this);
    this.bound_registerEventListeners = this.registerEventListeners.bind(this);
    this.bound_unregisterEventListeners = this.unregisterEventListeners.bind(this);
    this.bound_onMouseUp = this.onMouseUp.bind(this);
    this.bound_onMouseDown = this.onMouseDown.bind(this);
    this.bound_onMouseMove = this.onMouseMove.bind(this);
    this.bound_onKeyUp = this.onKeyUp.bind(this);
    this.bound_onKeyDown = this.onKeyDown.bind(this);
    this.bound_onZoomChange = this.onZoomChange.bind(this);
    this.bound_onModeChange = this.onModeChange.bind(this);
    this.bound_onCurvatureChange = this.onCurvatureChange.bind(this);
    this.bound_onFreqChange = this.onFreqChange.bind(this);
    this.bound_onPhaseChange = this.onPhaseChange.bind(this);
    this.bound_onShowControlPointsClick = this.onShowControlPointsClick.bind(this);
    
    this.bound_registerEventListeners();
    
    this.buildControlPanel();
  }
};
Painter.init();
Control.init();
